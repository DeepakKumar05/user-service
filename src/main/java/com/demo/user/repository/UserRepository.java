package com.demo.user.repository;

import com.demo.user.entity.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository  extends JpaRepository<UserDetails, Long> {

    @Query("SELECT ud from UserDetails ud where ud.name=?1")
    List<UserDetails> findUserByName(String name);
}
