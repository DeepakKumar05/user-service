package com.demo.user.service;

import com.demo.user.dto.UserDetailsDTO;
import com.demo.user.entity.UserDetails;
import com.demo.user.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    private final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;


    @Transactional
    public UserDetailsDTO saveUpdate(UserDetailsDTO userDetailsDTO){
        try{
            UserDetails userDetails = new UserDetails();
            userDetails.setName(userDetailsDTO.getName());
            userDetails.setAge(Integer.parseInt(userDetailsDTO.getAge()));
            LOGGER.info("Entering into user service to save user details::" + new Timestamp(new Date().getTime()));
            return getUserDetailsDTO(userRepository.save(userDetails));
        }catch(Exception e){
            LOGGER.warn("Exception in UserService -> saveUpdate();" +e);
        }
        return null;
    }

    public UserDetailsDTO getById(Long id){
        return getUserDetailsDTO(userRepository.getOne(id));
    }

    public List<UserDetailsDTO> getByName(String name){
        List<UserDetails> userDetailsList = userRepository.findUserByName(name);
        if(CollectionUtils.isEmpty(userDetailsList)){
            return null;
        }
        return  userDetailsList
                .stream()
                .map(this::getUserDetailsDTO)
                .collect(Collectors.toList());
    }

    public UserDetailsDTO getUserDetailsDTO(UserDetails userDetails){
        return new UserDetailsDTO(userDetails.getId().toString(),
                userDetails.getName(),
                userDetails.getAge().toString());
    }

}
